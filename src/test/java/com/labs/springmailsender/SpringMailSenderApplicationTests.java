package com.labs.springmailsender;

import freemarker.template.Configuration;
import freemarker.template.TemplateException;
import org.apache.commons.io.IOUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.*;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

@SpringBootTest
class SpringMailSenderApplicationTests {

	@Autowired
	private JavaMailSender javaMailSender;

	@Autowired
	private Configuration configuration;

	@Value("${MAIL_USER}")
	private String to;

	@Value("${MAIL_USER}")
	private String from;

	private String subject = "Spring Mail Sender Project";

	@Test
	void sendEmail() throws MessagingException, TemplateException, IOException {

		//Uncomment the method you want to run

		sendSimpleMail();
		//sendEmailWithAttachment();
		//SendMailBase64ImageAsAttachment();
		//sendMailWithFileAttachment();
		//sendMailFreemarker();
	}

	//This method sends a simple mail to "to"
	void sendSimpleMail() {

		SimpleMailMessage msg = new SimpleMailMessage();

		msg.setFrom(this.from);
		msg.setTo(this.to);
		msg.setSubject(this.subject);
		msg.setText("sendSimpleMail()");

		javaMailSender.send(msg);
	}

	void sendMailBase64ImageAsAttachment() throws MessagingException, IOException {
		MimeMessage msg = javaMailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(msg, true, "UTF-8");

		helper.setFrom(this.from);
		helper.setTo(this.to);
		helper.setSubject(this.subject);
		helper.setText("sendMailBase64ImageAsAttachment()");

		byte[] byteArrayFile = Base64.getDecoder().decode(getExampleBase64String().getBytes("UTF-8"));
		ByteArrayOutputStream baos = new ByteArrayOutputStream(byteArrayFile.length);

		baos.write(byteArrayFile, 0, byteArrayFile.length);
		InputStreamSource attachment = new ByteArrayResource(baos.toByteArray());

		helper.addAttachment("test.pdf", attachment, "application/pdf");

		javaMailSender.send(msg);
	}

	void sendMailWithFileAttachment() throws MessagingException {
		MimeMessage msg = javaMailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(msg, true, "UTF-8");

		helper.setFrom(this.from);
		helper.setTo(this.to);
		helper.setSubject(this.subject);
		helper.setText("sendMailWithFileAttachment()");

		helper.addAttachment("je.png",new File("src/main/resources/templates/img/je.png"));

		javaMailSender.send(msg);
	}

	void sendMailFreemarker() throws MessagingException, IOException, TemplateException {

		MimeMessage msg = javaMailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(msg, true, "UTF-8");

		helper.setFrom(this.from);
		helper.setTo(this.to);
		helper.setSubject(this.subject);

		StringWriter stringWriter = new StringWriter();
		Map<String, String> dataMap = new HashMap();
		dataMap.put("methodName","sendMailFreemarker()");

		configuration.getTemplate("freemarker-example.ftl").process(dataMap,stringWriter);
		helper.setText(stringWriter.toString(), true);

		javaMailSender.send(msg);
	}

	/*It is necessary to get the base64 String from a file otherwise we gonna receive the error "Constant string too long"*/
	String getExampleBase64String() throws IOException {
		FileInputStream fis = new FileInputStream("src/main/resources/base64.txt");
		String base64String = IOUtils.toString(fis, "UTF-8");
		return base64String;
	}
}